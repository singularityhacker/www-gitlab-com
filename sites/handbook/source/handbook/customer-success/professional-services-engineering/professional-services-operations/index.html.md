---
layout: handbook-page-toc
title: Professional Service Operations
category: Internal
description: "Learn about the GitLab Professional Services operations processes and workflows."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Professional Services Operations 
Professional Services Operations is a collaboration of our consulting, training, schedululing, reporting and backend processes to support the Professional Services team.


## Project Coordination- Consulting
Coming Soon, Project Coordination Processes

## Project Coordination- Training
Coming Soon, Project Coordination Processes

## Operations  
Coming Soon, Project Coordination Processes 

## Mavenlink Log In/ Password Creation
- At this time to log into Mavenlik, you will need to go to the Mavenlink landing page.
  - Our end goal is to have Mavenlink added to Okta


- First time logging in go to www.mavenlink.com
- Click on "Forget your Password" and follow the prompts to create your password
- Email will be sent to change your Mavenlink password
- Change your password
- Log in and Enjoy!

![](./Mavenlinklogin.png)

