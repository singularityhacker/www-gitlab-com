---
layout: handbook-page-toc
title: "Secure UX"
description: "The Secure UX team helps provide the best UX in taking pre-emptive security measures before deploying code. The Protect UX team helps provide the best UX in keeping your app safe after code is in production."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Overview
Secure tools help your team follow and enforce security best practices effortlessly as part of the DevOps cycle. The Secure UX team’s goal is to provide the best experience in taking pre-emptive security measures before deploying your code, while the [Protect UX](/handbook/engineering/ux/stage-group-ux-strategy/protect/) team’s goal is to provide the best experience in keeping your application safe after your code is in production. See the [Sec UX](/handbook/engineering/ux/stage-group-ux-strategy/sec/) page for more about our team and how our two teams work together.

### User
We have different user types we consider in our experience design effort. Even when a user has the same title, their responsibilities may vary by organization size, department, org structure, and role. Here are some of the people we are serving:

* [Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
* [Development Tech Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
* [DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
* [Security Analyst](/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
* Compliance Associate
* Security Engineer
* Chief Information Security Officer

Generally, developers are the users of the vulnerability reports in the MR/pipeline while security professionals are the users of the Security Dashboards.

### Jobs to be Done
We use the [Jobs to be Done (JTBDs) framework](https://about.gitlab.com/handbook/engineering/ux/jobs-to-be-done/) to keep us focused on user goals and to make sure we're supporting users on what they value. See a breakdown of the Secure and Protect Jobs to be Done [here](https://about.gitlab.com/handbook/engineering/development/secure/jtbd/index.html).

We also carry out [UX Scorecard evaluations](https://about.gitlab.com/handbook/engineering/ux/ux-scorecards/) for our JTBDs. See UX Scorecards for Secure and Protect, as well as for other stages, [here](https://gitlab.com/groups/gitlab-org/-/epics/1714).

### Team
* [Justin Mandell](https://gitlab.com/jmandell) - Product Design Manager
* [Tali Lavi](https://gitlab.com/tlavi) - UX Researcher
* [Kyle Mann](https://gitlab.com/kmann) - Sr. Product Designer
* [Andy Vople](https://gitlab.com/andyvolpe) - Sr. Product Designer
* [Camellia Yang](https://gitlab.com/cam.x) - Sr. Product Designer
* [Annabel Dunstone Gray](https://gitlab.com/annabeldunstone) - Product Designer
* [Becka Lippert](https://gitlab.com/beckalippert) - Product Designer

#### Team structure
We've divided the Secure stage into dedicated experience groups to align with a similar [split](/handbook/product/categories/#sec-section) undertaken by our engineering and PM counterparts.

**Security Testing**

| Group | Features                                                     | Designer(s)           |
| ---------------- | ------------------------------------------------------------ | --------------------- |
| Static Analysis | SAST, Secret Detection | Becka Lippert         |
| Dynamic Analysis | DAST | Annabel Dunstone Gray         |
| Fuzz Testing | Fuzz Testing | Camellia Yang         |


**Compliance & Auditing**

| Group            | Features                                                     | Designer(s)         |
| --------------------------- | ------------------------------------------------------------ | ------------------- |
| Composition Analysis       | Dependency Scanning, Container Scanning, License Compliance | Kyle Mann           |

**Threat Insights**

| Group            | Features                                                     | Designer(s)         |
| --------------------------- | ------------------------------------------------------------ | ------------------- |
| Vulnerability Management       | Vulnerability Management | Andy Volpe           |

The Secure & Protect UX teams work closely together and have shared coverage in the following areas:

- Security Dashboard
- Control Center
- Status, Metrics and Reporting
- Security Configuration

This segmentation gives us a better opportunity to:
- Grow our expertise and knowledge within our subgroup while sharing relevant information with the rest of the team.
- Evolve and maintain relationships with our dedicated engineering team and PMs.
- Serve as the known main point of contact.
- Deeply understand our users' needs by initiating and/or leading research activities specific to our experience group.
- Focus on iterating and progressing our experiences from MVC to Lovable.

Read more about how we've created these dedicated experience groups [here.](https://gitlab.com/gitlab-org/gitlab-design/issues/458)


### Follow our work
Our [Secure and Protect UX YouTube channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KrFCe5BgUkzFrZifjforQOz) includes UX Scorecard walkthroughs, UX reviews, group feedback sessions, team meetings, and more.
