backend:
 name: gitlab
 repo: gitlab-com/www-gitlab-com # Path to your GitLab repository
 auth_type: implicit # Required for implicit grant
 app_id: d656971f8c2a795cf94032efc60d55550ab37615bf4d07e919faea6eaa777d49 # Application ID from your GitLab settings
 cms_label_prefix: netlify-cms/
# These are default settings, define expclitly if different from defualt
# branch: master
# api_root: https://gitlab.com/api/v4
# base_url: https://gitlab.com
# auth_endpoint: oauth/authorize
# site_domain: location.hostname 
 
# Allows access to /admin/ on local dev server
# Run npx netlify-cms-proxy-server from the root directory in separate terminal window
# https://www.netlifycms.org/docs/beta-features/#working-with-a-local-git-repository
local_backend: true

publish_mode: editorial_workflow

media_folder: /source/images/blogimages
public_folder: /images/blogimages

collections:
  - name: topic
    label: Topic
    format: yml
    extension: yml
    folder: /data/topic/
    create: true
    slug: '{{file_name}}'
    fields:
      - {label: Title, name: title, hint: Title displayed in header of webpage. This title is meant for people that are already on your site. It’s telling them what your post or page is about., widget: string, required: true}
      - {label: SEO Title, hint:  is meant for people who are not on your website yet (viewing in SERP results). It will be shown to people in the search engines., name: seo_title, widget: string, required: false}
      - {label: SEO Description, hint: This is the SEO description that will get rendered in search results, name: description, widget: string, required: true}
      - {label: Header Body, name: header_body, widget: markdown, hint: This is displayed in the header underneath the title, required: true}
      - {label: Canonical Path, hint: Absolute path and it is always unique - please include the following `/topics/final-url-path` - replacing `final-url-path` with your parameter., name: canonical_path, widget: string, required: true}
      - {label: Final URL Path, hint: This will the be final part of the topics page url (/topics/FINAL-URL-PATH) and the name your data files is saved as in data/topic/. , name: file_name, widget: string, required: true}
      - label: Social share image
        hint: The images that is displayed in social media when this article is shared
        name: twitter_image
        widget: image
        media_folder: /source/images/opengraph
        public_folder: /images/opengraph
        required: true
      - label: Related Content, links in header to outside sources
        name: related_content
        hint: Links displayed in the header underneath the "MORE ON THIS TOPIC" title
        widget: list
        required: false
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: URL, name: url, widget: string, required: false}  
      - label: Cover Image
        hint: Image displayed at beginning of body content
        name: cover_image
        widget: image
        required: false
        media_folder: /source/images/topics
        public_folder: /images/topics
      - {label: Body, name: body, widget: markdown, required: true}
      - {label: Benefits Title, hint: Title displayed next to list of benefits for this topic,  name: benefits_title, widget: string, required: false}
      - {label: Benefits Description, name: benefits_description, widget: markdown, required: false}
      - label: benefits list
        name: benefits
        widget: list
        required: false
        hint: Each of these items will be displayed next to their image, underneath the body content
        fields:
          - {label: Title, name: title, widget: string, required: true}
          - {label: Description, name: description, widget: markdown, required: true}
          - {label: Image, name: image, widget: image, required: true, media_folder: /source/images/icons, public_folder: /images/icons}
      - {label: Second Benefits Title, hint: Title displayed next to list of benefits for this topic,  name: benefits_2_title, widget: string, required: false}
      - {label: Second Benefits Description, name: benefits_2_description, widget: markdown, required: false}
      - label: second benefits list
        name: benefits_2
        widget: list
        required: false
        hint: Second benefits block. Each of these items will be displayed next to their image, underneath the body content
        fields:
          - {label: Title, name: title, widget: string, required: true}
          - {label: Description, name: description, widget: markdown, required: true}
          - {label: Image, name: image, widget: image, required: true,  media_folder: /source/images/icons, public_folder: /images/icons}         
      - label: cta_banner
        name: cta_banner
        widget: list
        required: false
        hint: More content to be displayed after benefits of block, this content can have CTA buttons
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: CTA Body, name: body, widget: markdown, required: false}
          - label: CTA buttons
            name: cta
            widget: list
            required: false
            fields:
              - {label: CTA Text, name: text, widget: string, required: true}
              - {label: URL, name: url, widget: string, required: true}
      - {label: Resources title, hint: Title of resources block displayed at the bottom of the page, name: resources_title, widget: string, required: false}
      - {label: Resources description, name: resources_intro, widget: markdown, required: false}
      - label: Resources
        name: resources
        widget: list
        required: false
        hint: Each of these will be grouped by type and displayed in order as shown in the admin. You can drag and drop the items in the admin.
        fields:
          - {label: Title, name: title, widget: string, required: true}
          - {label: URL, name: url, widget: string, required: true}
          - {label: Type, name: type, widget: "select", options: ["Webcast", "Video", "Whitepapers", "Books", "Case studies", "Articles", "Reports", "Blog", "Podcast"], required: true}
      - label: Suggested Content
        name: suggested_content
        hint: Only place existing blog articles starting with the /blog/path-to-blog-article path. This block will intelligently pull the blog title, description, link, and image into a blog card displayed in a group at the bottom of the page.
        widget: list
        required: false
        fields:
          - {label: URL, name: url, widget: string}
      - label: schema.org FAQPage questions
        name: schema_faq
        hint: questions displayed in search results. DO NOT INCLUDE LINKS IN BODY. Use CTA button instead.
        widget: list
        fields:
          - {label: Question, name: question, widget: string}
          - {label: Answer, name: answer, widget: markdown, hint: DO NOT INCLUDE LINKS IN BODY, USE CTA BUTTON}
          - label: CTA buttons
            name: cta
            widget: list
            required: false
            fields:
              - {label: CTA Text, name: text, widget: string, required: false}
              - {label: URL, name: url, widget: string, required: false}
  - name: topic_child
    label: Topic Child Pages
    format: yml
    extension: yml
    folder: /data/topic_children/
    create: true
    slug: '{{file_name}}'
    fields:
      - {label: Title, name: title, hint: Title displayed in header of webpage. This title is meant for people that are already on your site. It’s telling them what your post or page is about., widget: string, required: true}
      - {label: SEO Title, hint:  is meant for people who are not on your website yet (viewing in SERP results). It will be shown to people in the search engines., name: seo_title, widget: string, required: false}
      - {label: SEO Description, hint: This is the SEO description that will get rendered in search results, name: description, widget: string, required: true}
      - {label: Header Body, name: header_body, widget: markdown, hint: This is displayed in the header underneath the title, required: true}
      - {label: Canonical Path, hint: Absolute path and it is always unique - please include the following `/topics/parent-topic/final-url-path` - replacing `parent-topic` and `final-url-path` with your parameters., name: canonical_path, widget: string, required: true}
      - {label: Final URL Path, hint: This will the be final part of the topics page url (/topics/parent-topic/FINAL-URL-PATH) and the name your data files is saved as in data/topic_children/. , name: file_name, widget: string, required: true}
      - { 
          label: 'Parent Topic',
          name: 'parent_topic',
          widget: 'relation',
          collection: 'topic',
          display_fields: [file_name],
          search_fields: [file_name],
          value_field: 'file_name',
          hint: This will the be preceding part of the topics page url - about.gitlab.com/parent-topic/final-url,
          required: true
        }
      - label: Social share image
        hint: The images that is displayed in social media when this article is shared
        name: twitter_image
        widget: image
        media_folder: /source/images/opengraph
        public_folder: /images/opengraph
        required: true
      - label: Related Content, links in header to outside sources
        name: related_content
        hint: Links displayed in the header underneath the "MORE ON THIS TOPIC" title
        widget: list
        required: false
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: URL, name: url, widget: string, required: false}
      - label: Cover Image
        hint: Image displayed at beginning of body content
        name: cover_image
        widget: image
        required: false
        media_folder: /source/images/topics
        public_folder: /images/topics
      - {label: Body, name: body, widget: markdown, required: true}
      - label: pull_quote
        name: pull_quote
        widget: list
        required: false
        hint: Pull quotes displayed after body content
        fields:
          - {label: quote, name: quote, widget: string, required: false}
          - {label: source, name: source, widget: string, required: false}
      - {label: Benefits Title, hint: Title displayed next to list of benefits for this topic,  name: benefits_title, widget: string, required: false}
      - {label: Benefits Description, name: benefits_description, widget: markdown, required: false}
      - label: benefits list
        name: benefits
        widget: list
        required: false
        hint: Each of these items will be displayed next to their image, underneath the body content
        fields:
          - {label: Title, name: title, widget: string, required: true}
          - {label: Description, name: description, widget: markdown, required: true}
          - {label: Image, name: image, widget: image, required: true, media_folder: /source/images/icons, public_folder: /images/icons}
      - {label: Second Benefits Title, hint: Title displayed next to list of benefits for this topic,  name: benefits_2_title, widget: string, required: false}
      - {label: Second Benefits Description, name: benefits_2_description, widget: markdown, required: false}
      - label: second benefits list
        name: benefits_2
        widget: list
        required: false
        hint: Second benefits block. Each of these items will be displayed next to their image, underneath the body content
        fields:
          - {label: Title, name: title, widget: string, required: true}
          - {label: Description, name: description, widget: markdown, required: true}
          - {label: Image, name: image, widget: image, required: true,  media_folder: /source/images/icons, public_folder: /images/icons}         
      - label: cta_banner
        name: cta_banner
        widget: list
        required: false
        hint: More content to be displayed after benefits of block, this content can have CTA buttons
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: CTA Body, name: body, widget: markdown, required: false}
          - label: CTA buttons
            name: cta
            widget: list
            required: false
            fields:
              - {label: CTA Text, name: text, widget: string, required: false}
              - {label: URL, name: url, widget: string, required: false}
      - {label: Resources title, hint: Title of resources block displayed at the bottom of the page, name: resources_title, widget: string, required: false}
      - {label: Resources description, name: resources_intro, widget: markdown, required: false}
      - label: Resources
        name: resources
        widget: list
        required: false
        hint: Each of these will be grouped by type and displayed in order as shown in the admin. You can drag and drop the items in the admin.
        fields:
          - {label: Title, name: title, widget: string, required: true}
          - {label: URL, name: url, widget: string, required: true}
          - {label: Type, name: type, widget: "select", options: ["Webcast", "Video", "Whitepapers", "Books", "Case studies", "Articles", "Reports", "Blog", "Podcast"], required: true}
      - label: Suggested Content
        name: suggested_content
        hint: Only place existing blog articles starting with the /blog/path-to-blog-article path. This block will intelligently pull the blog title, description, link, and image into a blog card displayed in a group at the bottom of the page.
        widget: list
        required: false
        fields:
          - {label: URL, name: url, widget: string}
      - label: schema.org FAQPage questions
        name: schema_faq
        hint: questions displayed in search results. DO NOT INCLUDE LINKS IN BODY. Use CTA button instead.
        widget: list
        fields:
          - {label: Question, name: question, widget: string}
          - {label: Answer, name: answer, widget: markdown, hint: DO NOT INCLUDE LINKS IN BODY, USE CTA BUTTON}
          - label: CTA buttons
            name: cta
            widget: list
            required: false
            fields:
              - {label: CTA Text, name: text, widget: string, required: false}
              - {label: URL, name: url, widget: string, required: false}
    view_filters:
      - label: "Agile Delivery"
        field: parent_topic
        pattern: 'agile-delivery'
      - label: "Application Security"
        field: parent_topic
        pattern: 'application-security'
      - label: "CI CD"
        field: parent_topic
        pattern: 'ci-cd'
      - label: "DevOps"
        field: parent_topic
        pattern: 'devops'
      - label: "GitOps"
        field: parent_topic
        pattern: 'gitops'
      - label: "Microservices"
        field: parent_topic
        pattern: 'microservices'
      - label: "Multicloud"
        field: parent_topic
        pattern: 'multicloud'
      - label: "Ops"
        field: parent_topic
        pattern: 'ops'
      - label: "Serverless"
        field: parent_topic
        pattern: 'serverless'
      - label: "Single Application"
        field: parent_topic
        pattern: 'single-application'
      - label: "Version Control"
        field: parent_topic
        pattern: 'version-control'
  - name: blog_posts
    label: Blog Posts
    preview_path: "blog/{{year}}/{{month}}/{{day}}/{{filename}}/"
    folder: /sites/marketing/source/blog/blog-posts/
    extension: .html.md.erb
    format: frontmatter
    create: true
    slug: '{{year}}-{{month}}-{{day}}-{{title}}'
    view_filters:
      - label: "Company"
        field: categories
        pattern: 'company'
      - label: "Culture"
        field: categories
        pattern: 'culture'
      - label: "Engineering"
        field: categories
        pattern: 'engineering'
      - label: "Insights"
        field: categories
        pattern: 'insights'
      - label: "News"
        field: categories
        pattern: 'news'
      - label: "Open Source"
        field: categories
        pattern: 'open source'
      - label: "Security"
        field: categories
        pattern: 'security'
      - label: "Unfiltered"
        field: categories
        pattern: 'unfiltered'
    fields:
      - {label: Title, name: title, widget: string, required: true}
      - {label: Author, name: author, widget: string, required: false}
      - {label: Author GitLab, name: author_gitlab, widget: string, required: false}
      - {label: Author Twitter, name: author_twitter, widget: string, required: false}
      - {label: Categories, name: categories, widget: string, required: true, hint: Visit https://about.gitlab.com/handbook/marketing/blog/#categories for all categories.}
      - {label: Tags, name: tags, widget: string, required: false, hint: Visit https://about.gitlab.com/handbook/marketing/blog/#tags see all available tags. Seperate by commas for multiple tags}
      - {label: Description, name: description, widget: string, required: true}
      - label: Image, 
        name: image_title 
        widget: image 
        required: false
        media_folder: /source/images/blogimages
        public_folder: /images/blogimages
      - {label: Twitter Text, name: twitter_text, widget: string, required: false}
      - {label: Body, name: body, widget: markdown, required: true}
  - name: typeform
    label: Typeform
    format: yml
    extension: yml
    folder: /data/typeform/
    create: true
    slug: '{{file_name}}'
    fields:
      - {label: Title, name: title, hint: Title displayed in header of webpage, widget: string, required: true}
      - {label: Header Body, name: header_body, widget: markdown, hint: This is displayed in the header underneath the title, required: true}
      - {label: SEO Description, hint: This is the SEO description that will get rendered in search results, name: description, widget: string, required: true}
      - {label: Final URL Path, hint: This will be the final part of the quiz page url and what your data file is named in data/typeform/ - about.gitlab.com/quiz/URL, name: file_name, widget: string, required: true}
      - label: Social share image
        hint: The images that is displayed in social media when this article is shared
        name: twitter_image
        widget: image
        media_folder: /source/images/opengraph
        public_folder: /images/opengraph
        required: true  
      - {label: Typeform embedd, name: typeform, widget: code, required: true}
      - label: cta_banner
        name: cta_banner
        widget: list
        required: false
        hint: More content to be displayed after benefits of block, this content can have CTA buttons
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: CTA Body, name: body, widget: markdown, required: false}
          - label: CTA buttons
            name: cta
            widget: list
            required: false
            fields:
              - {label: CTA Text, name: text, widget: string, required: true}
              - {label: URL, name: url, widget: string, required: true}
      - label: Suggested Content
        name: suggested_content
        hint: Only place existing blog articles starting with the /blog/path-to-blog-article path. This block will intelligently pull the blog title, description, link, and image into a blog card displayed in a group at the bottom of the page.
        widget: list
        required: false
        fields:
          - {label: URL, name: url, widget: string}
